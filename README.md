# main.clojure.sensorvisualizer

This project will read a file containing sensor data and display that data in an html file.

## dev setup

### Intellij 
Download community edition: http://www.jetbrains.com/idea/download/  
Then install cursive: https://cursiveclojure.com/userguide/ look at "Installing Cursive" on how to do this

#### Use Intellij
Start a repl from intellij, then open a file  
Move cursor to the form you want to evaluate, press CTRL+SHIFT+A then search for: "Run top form in repl". This way you can evaluate all forms in the repl.  
You might want to evaluate the (ns form first so that all references are loaded.  
  
For more info look here: https://cursiveclojure.com/userguide/repl.html


### lein-midje
`lein-midje` is a plugin, so add this to your
`~/.lein/profiles.clj`:

    {:user {:plugins [[lein-midje "3.1.1"]]}}


## Usage

    $ java -jar main.clojure.sensorvisualizer-0.1.0-standalone.jar [args]

### Create standalone Java application (JAR including dependencies)

    $ lein uberjar

### Run standalone Java application

    $ java -jar target/uberjar/sensorvisualizer-0.1.0-SNAPSHOT-standalone.jar



### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2014 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.