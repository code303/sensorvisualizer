(ns sensorvisualizer.parse_sensordata
  (:require
            [sensorvisualizer.config :as conf]
            [clojure.java.io :as io]
            [clojure.string :as str]))

(defn get-content-of-file [fname]
  (with-open [rdr (io/reader fname)]
        (doall (line-seq rdr))))


(defn data-line->map [line]
  (let [[ts temp hum] (str/split line #"\s+")]
    {:time ts :temp (read-string temp) :humidity (read-string hum)}))

(defn parse-file [file]
  (map data-line->map (get-content-of-file file))
    ;;(map (fn [line] (data-line->map line) ) (get-content-of-file file))
  )

(defn get-current-dataset
  ([] (get-current-dataset (conf/get-latest-sensor-file)))
  ([file] (last (parse-file file))))
