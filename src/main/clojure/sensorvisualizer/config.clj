(ns sensorvisualizer.config
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]
            [clojure.string :as str]))

(def ^:const sensorpath-key :sensorpath)
(def ^:const config-file-name "config.edn")

(defn from-edn [fname]
  "reads an edn file from classpath"
  (with-open [rdr (-> (io/resource fname)
                      io/reader
                      java.io.PushbackReader.)]
    (edn/read rdr)))

; retrieve file path from config
(defn get-test-sensor-file-path [config]
	(sensorpath-key config))

; introducing symbol to refer to loaded config file
(def sensor-data-file (get-test-sensor-file-path (from-edn config-file-name)))

(defn get-latest-sensor-file []
  sensor-data-file)

; this method should return the content line of a given file
; look here for different approaches: http://stackoverflow.com/questions/7756909/in-clojure-1-3-how-to-read-and-write-a-file
;; (defn get-content-of-file [fname]
;;    ;(slurp (io/resource fname))) ;reads the whole file - we'll need to go with second line based approach later
;;   (println fname)
;;   (with-open [rdr (io/reader fname)]
;;         (doseq [line (line-seq rdr)]
;;           (println line))))

