(ns sensorvisualizer.generate_html
    (:require [net.cgrand.enlive-html :as html]
              [clojure.java.io :as io]))


(def title-content {:title "conditions"})

(html/defsnippet panel-snippet "templates/panel-snippet.html"
                 [:.panel]
                 [value]
                 [:.digit :span] (html/content value))

(html/deftemplate main-template "templates/template.html"
                  [title temperature humidity]
                  [:title] (html/content title)
                  [:#temperature] (html/content (panel-snippet (str temperature "&deg;C")))
                  [:#humidity] (html/content (panel-snippet (str humidity "%")))
                  )




(defn write-html-file [html file]
  (let [filepath (str file "/index.html")]
    (with-open [wrtr (io/writer filepath)]
      (doseq [line html]
        (.write wrtr line)))))



(write-html-file (main-template "test1135" 12.2 66.6) "c:/tmp")

