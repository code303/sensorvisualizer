(ns sensorvisualizer.config_test
    (:require [sensorvisualizer.config :refer :all]
     [sensorvisualizer.parse_sensordata :refer :all]
     [sensorvisualizer.generate_html :refer :all]
     [midje.sweet :refer :all]))

; implement get-content-of-file to make this test work
(fact "get first line of sensors text"
      (get-content-of-file sensor-data-file) => '("2014-10-14T09:20:34.111 21.111111 65.22222222" "2014-10-14T09:25:34.222 22.111111 66.22222222" "2014-10-14T09:30:34.333 23.111111 67.22222222"))

(fact "convert data line to data map"
      (data-line->map "2014-10-14T09:20:34.111 21.111111 65.22222222") => {:time "2014-10-14T09:20:34.111" :temp 21.111111 :humidity 65.22222222})


(fact "parse file"
      (count (parse-file sensor-data-file)) => 3
      (str (type (first (parse-file sensor-data-file)))) => "class clojure.lang.PersistentArrayMap"
      )


(fact "get current dataset"
      (get-current-dataset) => {:time "2014-10-14T09:30:34.333" :temp 23.111111 :humidity 67.22222222}
      )
(fact "get current time"
      (get (get-current-dataset) :time) => "2014-10-14T09:30:34.333"
      )

;;
;;(fact "read html"
;;    (first (fetch-page)) => "abcde")

(fact "title"
      (title-content :title) => "conditions")

;;(fact "get html template"
;;      (main-template "test") => "test")

(fact "write html file"
      (write-html-file ["<html>\\ncontent\\n</html>"] "c:\\tmp"))

(fact "write html file with content"
      (write-html-file (main-template "test") "c:\\tmp"))

